package DiscordPostProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import LFG.Core.Post;
import discord4j.common.util.Snowflake;
import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.Message;
import interfaces.PostProvider;

public class DiscordPostProvider implements PostProvider{

	@Override
	public Collection<Post> getPost() {
		String token = "MTEwNTMwMjM0MzcyMzAxMjE2Nw.GeDVR3.ytSmyKYuOyVBuDNLUjVgIs0sZ5stOGNRdBIRlE";

        DiscordClient client = DiscordClient.create(token);
        GatewayDiscordClient gateway = client.login().block();

        discord4j.common.util.Snowflake channelId = Snowflake.of("1103745098744877128");

        discord4j.core.object.entity.channel.TextChannel channel = (discord4j.core.object.entity.channel.TextChannel) gateway.getChannelById(channelId).block();

        List<String> messageContents = new ArrayList<>();

        channel.getMessagesBefore(channel.getLastMessageId().orElse(null))
                .take(10)
                .map(Message::getContent)
                .doOnNext(messageContents::add)
                .doOnComplete(() -> {
                    System.out.println(messageContents);
                    gateway.logout().block();
                })
                .subscribe();
        System.out.println(messageContents);

        gateway.onDisconnect().block();
        List<Post> postList = new ArrayList<>();
        for (String messageContent : messageContents) {
			Post post = new Post(messageContent);
			postList.add(post);
		}
        
        //return messageContents;
        return postList;
	}

}

